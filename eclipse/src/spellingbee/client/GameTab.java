package spellingbee.client;

import spellingbee.network.Client;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene; 
import javafx.scene.layout.*; 
import javafx.stage.Stage; 
import javafx.scene.Group; 
import javafx.scene.control.*; 
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.*;



public class GameTab extends Tab{
	private Client currentClient;
	private Text scorefield;
	
	public GameTab(Client clientObject, ScoreTab obiwan) {
		super("Game");
		this.currentClient = clientObject;
		
		// Getting the letters and the center letter by calling the client
		String letters = currentClient.sendAndWaitMessage("getAllLetters");
		String centerLetter = currentClient.sendAndWaitMessage("getCenterLetter");
		
		
		
		// Creating the buttons and the boxes of the GUI
		Button letter1 = new Button(letters.substring(0, 1));
		Button letter2 = new Button(letters.substring(1, 2));
		Button letter3 = new Button(letters.substring(2, 3));
		Button letter4 = new Button(letters.substring(3, 4));
		Button letter5 = new Button(letters.substring(4, 5));
		Button letter6 = new Button(letters.substring(5, 6));
		Button letter7 = new Button(letters.substring(6));
		
		Button[] buttonArray = new Button[7];
		buttonArray[0] = letter1;
		buttonArray[1] = letter2;
		buttonArray[2] = letter3;
		buttonArray[3] = letter4;
		buttonArray[4] = letter5;
		buttonArray[5] = letter6;
		buttonArray[6] = letter7;
		
		HBox lettersHbox = new HBox();
		lettersHbox.getChildren().addAll(letter1, letter2, letter3, letter4, letter5, letter6, letter7);
		
		TextField wordInput = new TextField("");
		HBox input = new HBox();
		input.getChildren().addAll(wordInput);
		
		Button delete = new Button("Delete");
		Button clear = new Button("Clear");
		Button submit = new Button("Submit");
		HBox utilities = new HBox();
		utilities.getChildren().addAll(delete, clear, submit);
		
		TextField response = new TextField();
		TextField score = new TextField();
		HBox messageHbox = new HBox();
		messageHbox.getChildren().addAll(response, score);
		
		VBox newVbox = new VBox();
		newVbox.getChildren().addAll(lettersHbox, input, utilities, messageHbox);
	
		
		// Highlighting the center letter
		for(int i = 0; i < buttonArray.length; i++) {
			if(buttonArray[i].getText().equals(centerLetter)) {
				buttonArray[i].setStyle("-fx-text-fill: red");
			}
		}
		
		
		// Adding event handlers to the Score TextField and to the buttons
		score.setText("" + currentClient.sendAndWaitMessage("getScore"));
		score.textProperty().addListener(new ChangeListener<String>(){
			private ScoreTab gameScore = obiwan;

			@Override
			public void changed(ObservableValue<? extends String> arg0, String arg1, String arg2) {
				this.gameScore.refresh();
				System.out.println("hello");
			}
		});
		
		letter1.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter1.getText());
			}
		});
		letter2.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter2.getText());
			}
		});
		letter3.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter3.getText());
			}
		});
		letter4.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter4.getText());
			}
		});
		letter5.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter5.getText());
			}
		});
		letter6.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter6.getText());
			}
		});
		letter7.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText() + letter7.getText());
			}
		});
		
		
		
		
		delete.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText(wordInput.getText().substring(0, (wordInput.getText().length() - 1)));
			}
		});
		clear.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				wordInput.setText("");
			}
		});
		submit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent event) {
				String wordCheckResponse = currentClient.sendAndWaitMessage("wordCheck;" + wordInput.getText());
				String[] tempArray = wordCheckResponse.split(";");
				response.setText(tempArray[0]);
				score.setText(currentClient.sendAndWaitMessage("getScore"));
			}
		});
		
		this.setContent(newVbox);
	}
	
	/**
	 * getScoreField method for getting the value of the Score TextField
	 */
	public Text getScoreField() {
		return this.scorefield;
		
	}

}
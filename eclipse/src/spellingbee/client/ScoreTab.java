package spellingbee.client;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.*;
import java.io.*;
import java.util.*;
import spellingbee.network.*;
import javafx.scene.text.*;

/**
 * ScoreTab class that shows the score tab of the SpellingBeeClient, which displays the score brackets
 * @author Jackson Kwan
 *
 */
public class ScoreTab extends Tab{
	private Client sharedClient;
	private int[] scoreVal;
	
	private Text top1;
	private Text top2;
	private Text top3;
	private Text top4;
	private Text top5;
	private Text current;
	private Text currentpts;
	private int currentVal = 0;
	
	/**
	 * Main constructor that builds the tab
	 * @param input Client for networking
	 */
	public ScoreTab(Client input) {
		super("Score");
		sharedClient = input;
		//creating the tabs and text fields
		GridPane p = new GridPane();
		String brackets = sharedClient.sendAndWaitMessage("getBrackets");
		
		
		String[] stringBrackets = brackets.split(";");
		scoreVal = new int[stringBrackets.length];
		for (int i=0; i<stringBrackets.length; i++) {
			scoreVal[i] = Integer.parseInt(stringBrackets[i]);
		}
		
		top1 = new Text("Maximum");
		top2 = new Text("Bee-ast");
		top3 = new Text("Good");
		top4 = new Text("Okay");
		top5 = new Text("Bee Better");
		current = new Text("Current Score");
		
		Text pts1 = new Text(scoreVal[4] + "");
		Text pts2 = new Text(scoreVal[3] + "");
		Text pts3 = new Text(scoreVal[2] + "");
		Text pts4 = new Text(scoreVal[1] + "");
		Text pts5 = new Text(scoreVal[0] + "");
		currentpts = new Text(currentVal + "");
		
		//adding to the gridpane
		p.add(top1, 0,0,1,1);
		p.add(top2, 0,1,1,1);
		p.add(top3, 0,2,1,1);
		p.add(top4, 0,3,1,1);
		p.add(top5, 0,4,1,1);
		p.add(current, 0,5,1,1);
		
		p.add(pts1, 1,0,1,1);
		p.add(pts2, 1,1,1,1);
		p.add(pts3, 1,2,1,1);
		p.add(pts4, 1,3,1,1);
		p.add(pts5, 1,4,1,1);
		p.add(currentpts, 1,5,1,1);
		
		p.setHgap(10);
		p.setVgap(3);
		
		//setting colors
		top1.setFill(Color.GREY);
		top2.setFill(Color.GREY);
		top3.setFill(Color.GREY);
		top4.setFill(Color.GREY);
		top5.setFill(Color.GREY);
		currentpts.setFill(Color.RED);
		
		
		//setting the content
		this.setContent(p);	
	}
	
	/**
	 * Checks for the current score and updates the bracket colors depending on it
	 */
	public void refresh() {
		currentVal = Integer.parseInt(sharedClient.sendAndWaitMessage("getScore"));
		currentpts.setText(currentVal + "");
		
		if (currentVal >= scoreVal[0]) {
			top5.setFill(Color.BLACK);
		}
		if (currentVal >= scoreVal[1]) {
			top4.setFill(Color.BLACK);
		}
		if (currentVal >= scoreVal[2]) {
			top3.setFill(Color.BLACK);
		}
		if (currentVal >= scoreVal[3]) {
			top2.setFill(Color.BLACK);
		}
		if (currentVal >= scoreVal[4]) {
			top1.setFill(Color.BLACK);
		}
	}
}

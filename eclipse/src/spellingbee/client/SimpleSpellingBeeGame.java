package spellingbee.client;

public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	private int pointsForWord;
	private String message;
	private String allLetters;
	private char centerLetter;
	private int score;
	
	
	/**
	 * Hardcoded constructor for SimpleSpellingBeeGame that sets the 7 letters and the center letter and also initializes the score
	 */
	public SimpleSpellingBeeGame() {
		this.allLetters = "aedhlrn";
		this.centerLetter = 'a';
		this.score = 0;
	}

	
	/**
	 * getMessage method that validates the word attempt in order to return either "good" for a valid word or "bad" for an invalid word
	 */
	public String getMessage(String attempt) {
		if(!(attempt.contains(String.valueOf(this.centerLetter)))) {
			this.message = "bad";
			return this.message;
		}
		
		for(int i = 0; i < attempt.length(); i++) {
			if(attempt.charAt(i) == 'a') {
			}
			else {
				if(attempt.charAt(i) == 'e') {
				}
				else {
					if(attempt.charAt(i) == 'd') {
					}
					else {
						if(attempt.charAt(i) == 'h') {
						}
						else {
							if(attempt.charAt(i) == 'l') {
							}
							else {
								if(attempt.charAt(i) == 'r') {
								}
								else {
									if(attempt.charAt(i) == 'n') {
									}
									else {
										this.message = "bad";
										return this.message;
									}
								}
							}
						}
					}
				}
			}
		}
		
		this.message = "good";
		return this.message;
	}
	
	/**
	 * getPointsForWord doesn't check if the word contains other letters because getMessage already checks it and getMessage get called before getPointsForWord
	 * Although the order is different on the ServerController now
	 */
	public int getPointsForWord(String attempt) {
		if(this.message.equals("bad") || attempt.length() < 4) {
			this.pointsForWord = 0;
		}
		else {
			if(attempt.contains("a") && attempt.contains("e") && attempt.contains("d") && attempt.contains("h") && attempt.contains("l") && attempt.contains("r") && attempt.contains("n") ) {
				this.pointsForWord = attempt.length() + 7;
			}
			else if(attempt.length() == 4){
				this.pointsForWord = 1;
			}
			else {
				this.pointsForWord = attempt.length();
			}
		}
		this.score = this.score + this.pointsForWord;
		return this.pointsForWord;
	}

	
	/**
	 * getAllLetters method that returns the seven letters for the game
	 */
	public String getAllLetters() {
		return this.allLetters;
	}

	/**getCenterLetter that returns the center letter of the game
	 * 
	 */
	public char getCenterLetter() {
		return this.centerLetter;
	}

	/**
	 * getScore method that returns the current score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * getBrackets method that returns a hardcoded int array for the rankings
	 */
	public int[] getBrackets() {
		int[] ranks = {5,10,25,50,100};
		return ranks;

	}
	
}
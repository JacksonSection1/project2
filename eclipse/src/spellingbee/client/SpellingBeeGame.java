package spellingbee.client;
import java.util.*;
import java.nio.file.*;
import java.io.*;

/**
 * SpellingBeeGame class that has every method in order to play a version 
 * of the Spelling Bee Game that's featured on the NYTimes website
 * @author Jackson Kwan
 */
public class SpellingBeeGame implements ISpellingBeeGame{
	private String centerLetter;
	private int score;
	private ArrayList<String> foundWords = new ArrayList<String>();
	private List<String> possibleWords = createWordsFromFile("../../projectfiles/project/english.txt");
	private Random rand = new Random();
	private ArrayList<String> letterList = new ArrayList<String>();
	private ArrayList<String> allWords = new ArrayList<String>();
	private String msg = "";
	
	
	/**
	 * Constructor with no input, letters are taken from a file
	 */
	public SpellingBeeGame(){
		try {
			Path p = Paths.get("../../projectfiles/project/letterCombinations.txt");
			List<String> combinations = Files.readAllLines(p);
			String[] letters = combinations.get(rand.nextInt(combinations.size())).split("");
			for (String x : letters) {
				letterList.add(x);
			}
			centerLetter = letters[rand.nextInt(letters.length)];
			score = 0;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Constructor with input, letters taken as input
	 * @param input letter combination
	 */
	public SpellingBeeGame(String[] input){
			String []letters = input;
			for (String x : letters) {
				letterList.add(x);
			}
			centerLetter = letters[rand.nextInt(letters.length)];
			score = 0;
	}
	
	/**
	 * Creates a List of words from a file, given a path
	 * @param path path to the file
	 * @return List full of words
	 */
	public List<String> createWordsFromFile(String path){
		try {
			Path p  = Paths.get(path);
			List<String> returnList = Files.readAllLines(p);
			return returnList;
		}
		catch (IOException e) {
			e.printStackTrace();
			IllegalArgumentException error = new IllegalArgumentException("bad path");
			throw error;
		}
	}
	
	@Override
	/**
	 * Creates the brackets depending on the words that fit the letters given
	 * Brackets are made depending on the possibleWords List, that contains all the words in the english.txt file
	 * Override of the ISpellingBeeGame interface
	 * @return int[] with the brackets for this given set of letters and center letter
	 */
	public int[] getBrackets() {
		int maxPoints = 0;
		int[] returnArr = new int[5];
		for (int i=0; i<possibleWords.size(); i++) {
			String[] status = validateBracket(possibleWords.get(i)).split(";");
			if (status[0].equals("good")) {
				maxPoints = maxPoints + getPointsForWord(possibleWords.get(i));
			}
		}
		returnArr[0] = (int)(maxPoints * 0.25);
		returnArr[1] = (int)(maxPoints * 0.5);
		returnArr[2] = (int)(maxPoints * 0.75);
		returnArr[3] = (int)(maxPoints * 0.9);
		returnArr[4] = maxPoints;
		score = 0;
		return returnArr;
		
	}
	
	@Override
	/**
	 * Gets how many points a word is worth. 
	 * Override of the ISpellingBeeGame interface
	 * @param attempt word checked for points
	 * @return points the word is worth
	 */
	public int getPointsForWord(String attempt) {
		int pts = 0;
		boolean pangramCheck = true;
		String[] splitMsg = msg.split(";");
		String[] holder = attempt.split("");
		if (splitMsg[0].equals("bad")) {
			return pts;
		}
		else if (foundWords.contains(attempt)) {
			return pts;
		}
		else {
			ArrayList<String> wordList = createSplitWordList(holder);
			
			//checking for pangram
			for (int i=0; i<letterList.size(); i++) {
				if (!wordList.contains(letterList.get(i))) {
					pangramCheck = false;
					break;
				}
			}
			
			//point distribution
			if (pangramCheck) {
				pts = attempt.length() + 7;
				score = score + pts;
				return pts;
			}
			if (!pangramCheck && attempt.length() > 4) {
				pts = attempt.length();
				score = score + pts;
				return pts;
			}
			
			if (attempt.length() == 4) {
				pts = 1;
				score = score + pts;
				return pts;
			}
			return pts;
		}
	}
	
	/**
	 * Method that validates the words to create the brackets
	 * @param attempt word to check if it's a valid word
	 * @return string telling whether the attempt was good or bad
	 */
	public String validateBracket(String attempt) {
		String[] holder = attempt.split("");
		ArrayList<String> wordList = createSplitWordList(holder);
		
		//validation
		if (attempt.equals("")) {
			return "bad;empty";
		}
		for (int i=0; i<wordList.size(); i++) {
			if (!letterList.contains(wordList.get(i))) {
				return "bad;wrongLetter";
			}
		}
		if (attempt.length() < 4) {
			return "bad;lowerFour";
		}
		if (!wordList.contains(centerLetter)) {
			return "bad;noCenter";
		}
		
		else {
			allWords.add(attempt);
			return "good";
		}
	}
	
	@Override
	/**
	 * Method used to check the words for points during the game
	 * Override of the ISpellingBeeGame interface
	 * @param attempt word to be tested
	 * @return string whether the attempt was good or bad
	 */
	public String getMessage(String attempt) {
		if (foundWords.contains(attempt)) {
			msg = "bad;alreadyFound";
			return msg;
		}
		else if (!allWords.contains(attempt)) {
			msg = "bad;notAWord";
			return msg;
		}
		else {
			foundWords.add(attempt);
			msg = "good";
			return msg;
		}
	}
	
	@Override
	/**
	 * Gets the letters used in this run of the game
	 * Override of the ISpellingBeeGame interface
	 * @return 7 letters in the run
	 */
	public String getAllLetters() {
		String allLetters = "";
		for (String x : letterList) {
			allLetters = allLetters + x;
		}
		return allLetters;
	}
	
	@Override
	/**
	 * Returns the center letter
	 * Override of the ISpellingBeeGame interface
	 * @return center letter
	 */
	public char getCenterLetter() {
		return centerLetter.charAt(0);
	}
	
	@Override
	/**
	 * Returns the current score of the player
	 * Override of the ISpellingBeeGame interface
	 * @return score of the player
	 */
	public int getScore() {
		return score;
	}
	
	/**
	 * Creates an ArrayList of strings from the input String array
	 * @param input String array to be split
	 * @return ArrayList made from the input array
	 */
	public ArrayList<String> createSplitWordList(String[] input){
		ArrayList<String> returnList = new ArrayList<String>();
		for (String x : input) {
			returnList.add(x);
		}
		return returnList;
	}
	
}


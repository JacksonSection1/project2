package spellingbee.network;
import spellingbee.client.*;
import java.util.*;

/**
 * This class will run on the server side and is used to connect the server code to the backend business code.
 * This class is where the "protocol" will be defined.
 */
public class ServerController {
	private int points;
	private String[] splittedInput;
	private String message;
	// This is the interface you will be creating!
	// Since we are storing the object as an interface, you can use any type of ISpellingBeeGame object.
	// This means you can work with either the SpellingBeeGame OR SimpleSpellingBeeGame objects and can
	// seamlessly change between the two.
	//private String[] te = {"b", "a", "s", "h", "f", "u", "l" };
	private ISpellingBeeGame spellingBee = new SpellingBeeGame();
	
	/**
	 * Action is the method where the protocol translation takes place.
	 * This method is called every single time that the client sends a request to the server.
	 * It takes as input a String which is the request coming from the client. 
	 * It then does some actions on the server (using the ISpellingBeeGame object)
	 * and returns a String representing the message to send to the client
	 * @param inputLine The String from the client
	 * @return The String to return to the client
	 */
	public String action(String inputLine) {
		/* Your code goes here!!!!
		 * Note: If you want to preserve information between method calls, then you MUST
		 * store it into private fields.
		 * You should use the spellingBee object to make various calls and here is where your communication
		 * code/protocol should go.
		 * For example, based on the samples in the assignment:
		 * if (inputLine.equals("getCenter")) {
		 * 	// client has requested getCenter. Call the getCenter method which returns a String of 7 letters
		 *      return spellingBee.getCenter();
		 * }
		 * else if ( ..... )
		return null;*/
		
		// Server returning the center letter
		if (inputLine.equals("getCenterLetter")) {
			return String.valueOf(spellingBee.getCenterLetter());
		}
		
		// Server returning the 7 letters
		else if(inputLine.equals("getAllLetters")) {
			return spellingBee.getAllLetters();
		}
		
		// Server returning the current score
		else if (inputLine.equals("getScore")) {
			System.out.println("Score is : " + spellingBee.getScore());
			return spellingBee.getScore() + "";
		}
		
		// Server returning the brackets of the game for the score tab
		else if (inputLine.equals("getBrackets")) {
			int[] splitterArr = spellingBee.getBrackets();
			String holder = "";
			for (int i=0; i<splitterArr.length; i++) {
				holder = holder + splitterArr[i] + ";";
				System.out.println(splitterArr[i]);
			}

			System.out.println(holder);
			return holder;
		}
		
		// Server returning the proper message and the points of the attempt if the word is valid
		else if(inputLine.startsWith("wordCheck")) {
			this.splittedInput = inputLine.split(";");
			this.points = spellingBee.getPointsForWord(this.splittedInput[1]);
			this.message = spellingBee.getMessage(this.splittedInput[1]);
			return (this.message + ";" + this.points);
		}
		
		// In case of error/invalid server message sending (shouldn't happen though)
		else {
			return "error";
		}
	}
}
